### Development ###

Vagrant setup :

```
$ cat ~/.ssh/id_rsa.pub
$ echo "Add SSH key to Bitbucket"
$ mkdir ~/work
$ cd ~/work
$ git clone 'git@bitbucket.org:luc_phan/quoonel-httpclient-vagrant.git'
$ cd quoonel-httpclient-vagrant
$ vagrant up
$ vagrant ssh
```

Cloning :

```
$ ssh-keygen
$ cat ~/.ssh/id_rsa.pub
$ echo "Add SSH key to Bitbucket"
$ mkdir ~/work
$ cd ~/work
$ git clone 'git@bitbucket.org:luc_phan/quoonel-httpclient.git'
```

Testing :

```
$ cd ~/work/quoonel-httpclient
$ python -m unittest -v
$ python -m unittest -v test.test_httpclient.TestHttpClient.test_random_proxy
$ python -m unittest -v test.test_httpclient.TestHttpClient.test_random_useragent
$ python -m unittest -v test.test_httpclient.TestHttpClient.test_double_random
$ python -m unittest -v test.test_httpclient.TestHttpClient.test_database
$ python -m unittest -v test.test_proxylist
```