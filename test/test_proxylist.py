import unittest
import logging
import sys
from qhttpclient.proxylist import ProxyList
from qhttpclient.proxy import Proxy

class TestProxyList(unittest.TestCase):

    def setUp(self):
        #warnings.simplefilter('ignore')

        logger = logging.getLogger('qhttpclient')
        logger.setLevel(logging.DEBUG)
        console_handler = logging.StreamHandler(sys.stdout)
        logger.handlers = []
        logger.addHandler(console_handler)
        self._logger = logger

    def test_exists(self):
        logger = self._logger
        l = ProxyList()
        p = Proxy(ip="1.2.3.4", port=56789, protocol='http')
        self.assertFalse(l.proxy_exists(p))
        l.add_proxy(p)
        self.assertTrue(l.proxy_exists(p))

    def test_choose(self):
        logger = self._logger
        l = ProxyList()
        p = Proxy(ip="1.2.3.4", port=56789, protocol='http')
        l.add_proxy(p)
        p2 = l.choose()
        self.assertIs(p, p2)

    def test_iterator(self):
        logger = self._logger
        l = ProxyList()
        p = Proxy(ip="1.2.3.4", port=56789, protocol='http')
        l.add_proxy(p)
        for proxy in l:
            logger.debug(proxy)
            self.assertIsInstance(proxy, Proxy)

if __name__ == '__main__':
    unittest.main()
