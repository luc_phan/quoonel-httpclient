import unittest
import warnings
import logging
import sys
import json
import traceback
from sqlalchemy import create_engine
from qhttpclient import HttpClient, FreeProxyLists, SqlSh

class ProxyValidator:

    def __init__(self, http_client):
        self._httpclient = http_client
        self._logger = logging.getLogger('qhttpclient')
        self._logger.debug("Get reference origin...")
        self._origin = self._get_origin('http')

    def _get_origin(self, protocol, proxies=None):
        url = protocol + "://browsertest.quoonel.com/ip"
        h = self._httpclient
        r = h.get(url, proxies=proxies)
        if r.get_status_code() != 200:
            raise Exception("*** Failed getting origin!")
        j = json.loads(r.get_text())
        origin = j['origin']
        self._logger.debug("Origin: %s", origin)
        return origin

    def check_proxy(self, proxy):
        self._logger.debug("Checking proxy...")
        protocol = proxy.protocol
        try:
            origin = self._get_origin(protocol, proxies={protocol:proxy})
        except Exception as e:
            self._logger.debug(traceback.format_exc(chain=False))
            return False
        if self._origin in origin:
            return False
        return True

def check_response(response):
    return response.get_status_code() == 200

class TestHttpClient(unittest.TestCase):

    def setUp(self):
        warnings.simplefilter('ignore')

        logger = logging.getLogger('qhttpclient')
        logger.setLevel(logging.DEBUG)
        console_handler = logging.StreamHandler(sys.stdout)
        logger.handlers = []
        logger.addHandler(console_handler)
        self._logger = logger

        #self._engine = create_engine('sqlite:///:memory:', echo=True)
        self._engine = create_engine('sqlite:///:memory:')

        self._httpclient = HttpClient()

    def test_get(self):
        logger = self._logger
        h = self._httpclient

        r = h.get("http://browsertest.quoonel.com/user-agent")
        logger.debug(r.get_text())
        self.assertEqual(r.get_status_code(), 200)

    def test_crawl_proxies(self):
        logger = self._logger
        h = self._httpclient

        proxy_crawler = FreeProxyLists(h)
        proxies = proxy_crawler.search_proxies()
        self.assertGreater(len(proxies), 0)

        count = h.extend_proxies(proxies)
        self.assertGreater(count, 0)

        count = h.extend_proxies(proxies)
        self.assertEqual(count, 0)

    def test_crawl_useragents(self):
        logger = self._logger
        h = self._httpclient

        useragent_crawler = SqlSh(h)
        useragents = useragent_crawler.search_useragents()
        self.assertGreater(len(useragents), 0)

        count = h.extend_useragents(useragents)
        self.assertGreater(count, 0)

        count = h.extend_useragents(useragents)
        self.assertEqual(count, 0)

    def test_random_proxy(self):
        logger = self._logger
        self.test_crawl_proxies()
        h = self._httpclient

        h.config(https_verify=False)
        h.init_browser()
        h.config(
            check_proxy = ProxyValidator(h).check_proxy,
            proxy_tries = 20,
            request_tries = 10,
            use_random_proxy = True
        )
        h.init_browser()

        r = h.get("http://httpbin.org/ip")
        logger.debug(r.get_text())
        self.assertEqual(r.get_status_code(), 200)

    def test_random_useragent(self):
        logger = self._logger
        self.test_crawl_useragents()
        h = self._httpclient

        h.config(use_random_useragent=True)
        h.init_browser()

        r = h.get("http://httpbin.org/user-agent")
        logger.debug(r.get_text())
        self.assertEqual(r.get_status_code(), 200)

    def test_double_random(self):
        logger = self._logger
        h = self._httpclient
        h.config(check_response=check_response)

        self.test_random_proxy()
        self.test_random_useragent()

        r = h.get("http://browsertest.quoonel.com/ip")
        logger.debug(r.get_text())
        self.assertEqual(r.get_status_code(), 200)

        r = h.get("https://browsertest.quoonel.com/ip")
        logger.debug(r.get_text())
        self.assertEqual(r.get_status_code(), 200)

        r = h.get("http://browsertest.quoonel.com/user-agent")
        logger.debug(r.get_text())
        self.assertEqual(r.get_status_code(), 200)

        r = h.get("https://browsertest.quoonel.com/user-agent")
        logger.debug(r.get_text())
        self.assertEqual(r.get_status_code(), 200)

    def test_database(self):
        logger = self._logger
        self.test_crawl_proxies()
        self.test_crawl_useragents()
        h = self._httpclient
        h.init_database(self._engine)
        h.save()

        h2 = HttpClient()
        h2.init_database(self._engine)
        h2.load()
        h2.config(https_verify=False)
        h2.init_browser()
        h2.config(
            check_response = check_response,
            use_random_useragent = True,
            check_proxy = ProxyValidator(h2).check_proxy,
            proxy_tries = 20,
            request_tries = 10,
            use_random_proxy = True
        )
        h2.init_browser()

        r = h2.get("http://httpbin.org/ip")
        logger.debug(r.get_text())
        self.assertEqual(r.get_status_code(), 200)

        r = h2.get("https://httpbin.org/ip")
        logger.debug(r.get_text())
        self.assertEqual(r.get_status_code(), 200)

        r = h2.get("http://httpbin.org/user-agent")
        logger.debug(r.get_text())
        self.assertEqual(r.get_status_code(), 200)

        r = h2.get("https://httpbin.org/user-agent")
        logger.debug(r.get_text())
        self.assertEqual(r.get_status_code(), 200)

    def test_cache(self):
        logger = self._logger
        h = HttpClient()

if __name__ == '__main__':
    unittest.main()
