from .weightedlistable import WeightedListable

class WeightedUserAgent(WeightedListable):

    def get_signature(self):
        return self.element.string

    def get_weight(self):
        return self.element.popularity

    def set_weight(self, weight):
        self.element.popularity = weight
