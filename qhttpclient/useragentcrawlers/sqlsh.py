import logging
import xml.etree.ElementTree as ET
from ..useragent import UserAgent
from ..useragentcrawler import UserAgentCrawler

class SqlSh(UserAgentCrawler):

    url = "http://sql.sh/ressources/sql-user-agents/user_agents.xml"

    def __init__(self, http_client):
        self._logger = logging.getLogger(__name__)
        self._http_client = http_client

    def search_useragents(self):
        useragents = []
        h = self._http_client
        r = h.get(self.url)
        root = ET.fromstring(r.get_text())
        for table in root.findall('.//table'):
            string = table.find('column[@name="useragents"]').text
            popularity = int(table.find('column[@name="popularity"]').text)
            u = UserAgent(string=string, popularity=popularity)
            useragents.append(u)
        self._logger.debug("User-agents crawled: %d", len(useragents))
        return useragents
