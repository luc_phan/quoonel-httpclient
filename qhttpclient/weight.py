class Weight:
    def get_weight(self):
        raise NotImplemented("Not implemented: get_weight()")
    def set_weight(self, weight):
        raise NotImplemented("Not implemented: set_weight()")
