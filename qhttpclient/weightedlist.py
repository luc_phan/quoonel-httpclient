import random
import logging
import pprint
from .exceptions import RandomChoiceFailed

class WeightedList:

    def __init__(self):
        self._logger = logging.getLogger(__name__)
        self._list = {}

    def exists(self, element):
        signature = element.get_signature()
        return signature in self._list

    def add(self, element):
        signature = element.get_signature()
        self._list[signature] = element

    def choose(self):
        l = self._list
        m = min(
            [weight.get_weight() for signature, weight in l.items()]
        )
        total = sum(
            [weight.get_weight() - m + 1 for signature, weight in l.items()]
        )
        r = random.randint(1, total)
        dw = 0
        for signature, weight in l.items():
            dw += weight.get_weight() - m + 1
            if r <= dw:
                return weight.element
        raise RandomChoiceFailed()

    def __iter__(self):
        for signature, weight in self._list.items():
            yield weight.element
