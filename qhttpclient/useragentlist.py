from .weightedlist import WeightedList
from .weighteduseragent import WeightedUserAgent

class UserAgentList(WeightedList):

    def useragent_exists(self, useragent):
        wua = WeightedUserAgent(useragent)
        return self.exists(wua)

    def add_useragent(self, useragent):
        wua = WeightedUserAgent(useragent)
        self.add(wua)
