import logging
from bs4 import BeautifulSoup
import re
import urllib
from qhttpclient.proxycrawler import ProxyCrawler
from qhttpclient.proxy import Proxy

class FreeProxyLists(ProxyCrawler):

    url = "http://freeproxylists.com/fr.html"

    def __init__(self, http_client):
        self._logger = logging.getLogger(__name__)
        self._http_client = http_client

    def search_proxies(self):
        logger = self._logger
        proxies = []
        h = self._http_client
        r = h.get(self.url)
        soup = BeautifulSoup(r.get_text())
        th = soup.find('th', text='raw proxy list')
        table = th.parent.parent
        links = table.find_all('a', text=re.compile('detailed list'))
        for link in links:
            href = link['href']
            url = urllib.parse.urljoin(self.url, href)
            r = h.get(url)
            soup = BeautifulSoup(r.get_text())
            body = soup.find('body')
            m = re.match("loadData\('dataID', '(.*)'\);", body['onload'])
            if not m:
                raise Exception('No loadData')
            url = urllib.parse.urljoin(url, m.group(1))
            r = h.get(url)
            soup = BeautifulSoup(r.get_text())
            quote = soup.find('quote')
            soup = BeautifulSoup(quote.get_text())
            trs = soup.find_all('tr')
            for tr in trs:
                tds = tr.find_all('td')
                if not tds:
                    continue
                m = re.match('\d+\.\d+\.\d+\.\d+', tds[0].get_text())
                if not m:
                    continue
                ip = tds[0].get_text()
                port = tds[1].get_text()
                protocol = 'http' if tds[2].get_text() == 'false' else 'https'
                proxy = Proxy(
                    ip=ip,
                    port=port,
                    protocol=protocol
                )
                proxies.append(proxy)
        logger.debug("Proxies crawled: %d", len(proxies))
        return proxies
