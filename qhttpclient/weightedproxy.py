import json
import hashlib
from .weightedlistable import WeightedListable

class WeightedProxy(WeightedListable):

    def get_signature(self):
        d = self.element.get_dict()
        j = json.dumps(d, sort_keys=True)
        md5 = hashlib.md5(j.encode('utf-8')).hexdigest()
        return md5

    def get_weight(self):
        return self.element.score

    def set_weight(self, weight):
        self.element.score = weight
