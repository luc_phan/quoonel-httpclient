class Response:

    def __init__(self, requests_response):
        self.requests_response = requests_response

    def get_text(self):
        return self.requests_response.text

    def get_status_code(self):
        return self.requests_response.status_code

    def __repr__(self):
        return "<Response({})>".format(self.get_status_code())
