import logging
from .weightedlist import WeightedList
from .weightedproxy import WeightedProxy

class ProxyList(WeightedList):

    def proxy_exists(self, proxy):
        wp = WeightedProxy(proxy)
        return self.exists(wp)

    def add_proxy(self, proxy):
        wp = WeightedProxy(proxy)
        self.add(wp)

    def promote(self, proxy):
        logger = logging.getLogger(__name__)
        proxy.score += len(self._list)
        logger.debug("Proxy promoted: %s", proxy.get_url())
        self._display_proxies()

    def demote(self, proxy):
        logger = logging.getLogger(__name__)
        proxy.score -= len(self._list)
        logger.debug("*** Proxy demoted: %s", proxy.get_url())
        self._display_proxies()

    def _display_proxies(self):
        logger = logging.getLogger(__name__)
        logger.debug("Proxy list:")
        l = self._list
        for s in sorted(l.keys(), key=lambda s: l[s].get_weight()):
            proxy = l[s]
            #logger.debug("%d: %s", proxy.get_weight(), proxy.element.get_dict())
            #logger.debug("%d: %s", proxy.get_weight(), repr(proxy.element))
            logger.debug("%d: %s", proxy.get_weight(), proxy.element.get_url())
