import logging
import traceback
from sqlalchemy.orm import sessionmaker
from .browser import Browser
from .proxylist import ProxyList
from .useragentlist import UserAgentList
from .weightedproxy import WeightedProxy
from .weighteduseragent import WeightedUserAgent
from .exceptions import RequestFailed, NoValidProxy
from .base import Base
from .proxy import Proxy
from .useragent import UserAgent
from .config import Config

class HttpClient:

    def __init__(self, **kwargs):
        self._logger = logging.getLogger(__name__)
        self._database_session = None
        self._proxy_lists = {
            'http': ProxyList(),
            'https': ProxyList()
        }
        self._useragent_list = UserAgentList()
        self._config = Config(**kwargs)
        self.init_browser()

    def get(self, url, **kwargs):
        return self.request('GET', url, **kwargs)

    def request(self, method, url, **kwargs):
        logger = self._logger
        config = self._config

        if 'proxies' in kwargs:
            return self._browser.request(
                method,
                url,
                proxies=kwargs['proxies']
            )
        elif not config.use_random_proxy:
            return self._browser.request(method, url)
        else:
            protocol = 'https' if url.startswith('https://') else 'http'
            l =  self._proxy_lists[protocol]
            tries = config.request_tries
            while tries:
                logger.debug("Request tries: %d left", tries)
                tries -= 1
                b = self._browser
                proxy = b.proxies[protocol]
                try:
                    r = b.request(method, url)
                    if config.check_response:
                        if config.check_response(r):
                            l.promote(proxy)
                        else:
                            l.demote(proxy)
                            raise RequestFailed()
                    return r
                except Exception as e:
                    logger.debug(traceback.format_exc(chain=False))
                    l.demote(proxy)
                    b2 = self.init_browser(b.proxies)
                    b2.proxies[protocol] = self._choose_valid_proxy(protocol)

        raise RequestFailed()

    def extend_proxies(self, proxies):
        count = 0
        for proxy in proxies:
            protocol = proxy.protocol
            proxy_list = self._proxy_lists[protocol]
            if not proxy_list.proxy_exists(proxy):
                proxy_list.add_proxy(proxy)
                count += 1
        self._logger.debug("Proxies added: %d", count)
        return count

    def extend_useragents(self, useragents):
        count = 0
        for useragent in useragents:
            useragent_list = self._useragent_list
            if not useragent_list.useragent_exists(useragent):
                useragent_list.add_useragent(useragent)
                count += 1
        self._logger.debug("User-agents added: %d", count)
        return count

    def config(self, **kwargs):
        config = self._config
        config.extend(**kwargs)

    def init_browser(self, proxies=None):
        config = self._config
        b = Browser(https_verify=config.https_verify)

        if proxies:
            b.proxies = proxies
        elif config.use_random_proxy:
            b.proxies = self._choose_valid_proxies()

        if config.use_random_useragent:
            b.set_useragent(self._choose_useragent())

        self._browser = b
        return b

    def _choose_valid_proxies(self):
        proxies = {}
        for protocol in ('http', 'https'):
            proxies[protocol] = self._choose_valid_proxy(protocol)
        return proxies

    def _choose_valid_proxy(self, protocol):
        config = self._config
        l = self._proxy_lists[protocol]
        tries = config.proxy_tries
        while tries:
            self._logger.debug("Proxy tries: %d left", tries)
            tries -= 1
            proxy = l.choose()
            if not config.check_proxy:
                return proxy
            if config.check_proxy(proxy):
                l.promote(proxy)
                return proxy
            l.demote(proxy)
        raise NoValidProxy()

    def _choose_useragent(self):
        l = self._useragent_list
        return l.choose()

    def init_database(self, engine):
        Base.metadata.create_all(engine)
        Session = sessionmaker(bind=engine)
        self._database_session = Session()

    def save(self):
        logger = self._logger
        s = self._database_session
        logger.debug("Saving proxies...")
        for protocol in ('http', 'https'):
            for proxy in self._proxy_lists[protocol]:
                s.add(proxy)
        logger.debug("Saving user-agents...")
        for useragent in self._useragent_list:
            s.add(useragent)
        s.commit()

    def load(self):
        logger = self._logger
        s = self._database_session

        proxies = s.query(Proxy).all()
        count = self.extend_proxies(proxies)
        logger.debug("Proxies loaded: %d", count)

        useragents = s.query(UserAgent).all()
        count = self.extend_useragents(useragents)
        logger.debug("User-agents loaded: %d", count)
