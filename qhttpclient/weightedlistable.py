from .signed import Signed
from .weight import Weight

class WeightedListable(Signed, Weight):

    def __init__(self, element):
        self.element = element
