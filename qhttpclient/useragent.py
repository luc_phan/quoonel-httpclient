from qhttpclient.base import Base
from sqlalchemy import Column, Sequence, Integer, String

class UserAgent(Base):

    __tablename__ = 'useragents'
    id = Column(Integer, Sequence('useragents_id_seq'), primary_key=True)
    string = Column(String(256), unique=True)
    popularity = Column(Integer)

    def __repr__(self):
        return "<UserAgent(string='{}',popularity='{}')>".format(
            self.string,
            self.popularity
        )
