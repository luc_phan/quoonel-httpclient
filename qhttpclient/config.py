class Config:

    _attributes = {
        'https_verify': True,
        'check_response': None,
        'use_random_proxy': False,
        'request_tries': 3,
        'check_proxy': None,
        'proxy_tries': 5,
        'use_random_useragent': False
    }

    def __init__(self, **kwargs):
        for a in self._attributes:
            if a in kwargs:
                setattr(self, a, kwargs[a])
            else:
                setattr(self, a, self._attributes[a])

    def extend(self, **kwargs):
        for a in self._attributes:
            if a in kwargs:
                setattr(self, a, kwargs[a])
