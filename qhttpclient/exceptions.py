class RequestFailed(Exception):
    pass

class RandomChoiceFailed(Exception):
    pass

class NoValidProxy(Exception):
    pass
