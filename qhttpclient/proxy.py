from qhttpclient.base import Base
from sqlalchemy import Column, Sequence, Integer, String, UniqueConstraint
from sqlalchemy.orm import relationship

class Proxy(Base):

    __tablename__ = 'proxies'
    id = Column(Integer, Sequence('proxy_id_seq'), primary_key=True)
    ip = Column(String(16))
    port = Column(Integer)
    protocol = Column(String(8))
    score = Column(Integer, default=0)
    __table_args__ = ( UniqueConstraint('ip','port','protocol'), )

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.score = 0

    def __repr__(self):
        return "<Proxy(ip='{}',port='{}',protocol='{}',score='{}')>".format(
            self.ip,
            self.port,
            self.protocol,
            self.score
        )

    def get_url(self):
        return '{}://{}:{}'.format(self.protocol, self.ip, self.port)

    def get_dict(self):
        return {self.protocol: self.get_url()}
