import requests
import logging
from .response import Response

class Browser:

    def __init__(self, https_verify=True):
        self._logger = logging.getLogger(__name__)
        self._requests_session = requests.Session()
        self.https_verify = https_verify
        self.proxies = {}

    def set_useragent(self, useragent):
        self._requests_session.headers['User-Agent'] = useragent.string

    def request(self, method, url, **kwargs):
        logger = self._logger
        s = self._requests_session

        proxies = self.proxies
        if 'proxies' in kwargs:
            proxies = kwargs['proxies']
        proxies_dict = self._get_proxies_dict(proxies)

        logger.debug(
            "%s %s proxies=%s https_verify=%s",
            method,
            url,
            proxies_dict,
            self.https_verify
        )
        requests_response = s.request(
            method, url, proxies=proxies_dict, verify=self.https_verify
        )
        response = Response(requests_response)

        logger.debug(response)
        logger.debug(response.requests_response.history)
        return response

    def _get_proxies_dict(self, proxies):
        if proxies is None:
            return None
        return {protocol:proxy.get_url() for protocol, proxy in proxies.items()}
