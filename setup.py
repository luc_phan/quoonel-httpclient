from setuptools import setup, find_packages

setup(
    name="quoonel-httpclient",
    version="0.3.0",
    packages=find_packages(),
    install_requires=[
        "beautifulsoup4",
        "sqlalchemy",
        "requests"
    ]
)
